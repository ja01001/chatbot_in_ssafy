import re
import urllib.request
import time
from bs4 import BeautifulSoup
from urllib import parse
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import json
from datetime import datetime
from operator import itemgetter
from matplotlib import pyplot as plt
import matplotlib.font_manager as fm

with open('key.json') as f :
    JSON_DATA = json.load(f)

SLACK_TOKEN = JSON_DATA['SLACK_TOKEN']
SLACK_SIGNING_SECRET = JSON_DATA['SLACK_SIGNING_SECRET']
TOKENs = JSON_DATA['TOKENs']

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
def draw_graph(x_data,y_data):
    time_data = x_data
    value_data = y_data
    font = fm.FontProperties(fname='./NanumBarunGothic.ttf')
    plt.figure(figsize=(len(time_data), max(value_data)))
    plt.xlabel('time (hour)')
    plt.ylabel('density (㎍/m)',fontproperties = font)
    plt.bar(time_data, value_data, align="center")
    plt.title('00시부터 현재까지의 미세먼지 그래프',fontproperties = font)
    plt.savefig('test.png', format='png')
    pic = 'test.png'

    return pic
def make_msg(*parameter) :
    msg = ""
    text = parameter[0]
    mol = parameter[1]
    file_name= parameter[2]
    message=parameter[3]
    cho= parameter[4]
    cho_mes=parameter[5]

    if message == '1' and mol != "-":
        msg += "금일 %s의 미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다.\n" % (text, mol, '좋음')
        file_name = 'jjoum.png'
    elif message == '2' and mol != "-":
        msg += "금일 %s의 미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. \n" % (text, mol, '보통')
        file_name = 'bottong.png'
    elif message == '3' and mol != "-":
        msg += "금일 %s의 미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. \n" % (text, mol, '나쁨')
        file_name = 'dulssiham.png'
    elif message == '4' and mol != "-":
        msg += "금일 %s의 미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. \n" % (text, mol, '매우나쁨')
        file_name = 'simham.png'
    else:
        msg = "현재측정값이 존재하지 않습니다."
    if cho_mes == '1' and cho != "-":
        msg += "금일 %s의 초미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. " % (text, cho, '좋음')
    elif cho_mes == '2' and cho != "-":
        msg += "금일 %s의 초미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. " % (text, cho, '보통')
    elif cho_mes == '3' and cho != "-":
        msg += "금일 %s의 초미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. " % (text, cho, '나쁨')
    elif cho_mes == '4' and cho != "-":
        msg += "금일 %s의 초미세먼지 농도 예상값은 *%s㎍/m³* 로 *'%s'* 입니다. " % (text, cho, '매우나쁨')

    else:
        msg = "현재측정값이 존재하지 않습니다."
    return msg , file_name
# 크롤링 함수 구현하기
def _crawl(text,channel):
    text = text.split(" ")[1]
    # 여기에 함수를 구현해봅시다.
    test = 'http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty?serviceKey='
    urls = test + TOKENs + '&numOfRows=24&pageNo=1&stationName=' + parse.quote(text) + '&dataTerm=DAILY&ver=1.3'
    source_code = urllib.request.urlopen(urls).read()
    soup = BeautifulSoup(source_code, "html.parser")
    msg = ""
    try:
        message = soup.find("pm10grade").get_text()
        mol = soup.find("pm10value24").get_text()
        cho = soup.find("pm25value24").get_text()
        cho_mes = soup.find("pm10grade").get_text()
        value_data = []
        time_data = []
        tuple_data = []

        for e in soup.findAll('pm10value'):
            value_data.append(e.get_text())
        for e in soup.findAll('datatime'):
            time_data.append(e.get_text())
        for e in range(len(time_data)):
            tuple_data.append((time_data[e][11:],value_data[e]))
        sorted(tuple_data,key = itemgetter(0))
        dt = datetime.now()
        value_data.clear()
        time_data.clear()
        real_data = tuple_data[:dt.hour+1]
        for i in real_data[::-1]:
            time_data.append(i[0])
            if i[1] == '-':
                value_data.append(0)
            else :
                value_data.append(int(i[1]))
        pic = draw_graph(time_data,value_data)

        file_name = ''
        msg , file_name = map(str,make_msg(text, mol, file_name, message, cho, cho_mes))

    except AttributeError as e:
        msg = "정확한 구를 입력해 주세요 "
    except TimeoutError as e :
        print(e)
    except UnboundLocalError as e :
        pass
    else :
        pass
    slack_web_client.chat_postMessage(channel=channel, text=msg)
    try:
        if message[0] != "현재측정값이 존재하지 않습니다." and text != None:
            slack_web_client.files_upload(channels=channel, file=pic,title= "오늘 현시간까지 측정값 그래프")
            msg = ""
            slack_web_client.files_upload(channels=channel, file=file_name)
            text = None
            message = None
        else:
            text = None
            pass
    except UnboundLocalError as e :
        pass
    except IndexError as e :
        pass
    else :
        pass
# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    second_5 = int(time.mktime(datetime.today().timetuple()))
    try :
        if second_5 - 3 < int(event_data["event_time"]):

            channel = event_data["event"]["channel"]
            text = event_data["event"]["text"]
            _crawl(text, channel)
        else:
            pass
    except RuntimeError as e:
        pass
    # event_time 들어온시간 SECOND5 가 지금시간




# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8080)
